const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');


module.exports = {
  entry: './src/index.jsx',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  resolve:{
    extensions: ['.js', '.json', '.jsx'],

  },

  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", 
                  { loader: "css-loader", 
                  options: {
                    modules: {
                      localIdentName: "[path][name]__[local]--[hash:base64:5]",
                      localIdentContext: path.resolve(__dirname, "src"),
                   
                      exportLocalsConvention: "camelCase",
                     
                                                    },
                                                },
                                              },
                
                                            ]
                    
                                          }, 



                    {
                      test: /\.jsx$/,
                      exclude: /node_modules/,
                      use: {
                        loader: 'babel-loader',
                        options: {
                          presets: [
                            
                            '@babel/preset-env',
                            '@babel/preset-react'
                          ]
                                 }    
                    
                            },
                          },
                          {
                            exclude: "/node_modules/",
                            test: /\.png$/,
                            use: ['file-loader'],
                        },                             
                
                
                
                
                
                
                
                 
    ],
  },
  plugins:  [new HtmlWebpackPlugin(
    {
      template: './public/index.html',
    }
  )], 
};