import React from 'react';
import Header from '../header';
import style from "./style.css";
import image from '../../public/image.png'



//const wrapper = document.createElement('div');
//const h1 = document.createElement('h1');
//h1.classList.add(style.appHeader);

//const headerText  =  document.createTextNode('Hello World - 2');

//h1.append(headerText);

//wrapper.append(header, h1)

//const Page = React.createElement('div', null, <Header/>, React.createElement('h1', {className: style.appHeader}, 'Hello world - !!!'));

const H1 = () => (
      <h1 className={style.appHeader}>
          Hello React !!!!
      </h1>    
)

const Image = () => (
    <img src={image} alt={"image"} className={style.imageStyle}/>
)



const Page = () => (
    <div>
        <Header/>
        <H1/>
        <Image/>
    </div>    
)


export default Page;